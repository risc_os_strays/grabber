#!/usr/bin/lua
--[[
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
--]]
--This will only work in Linux!
--require 'lfs' --no.

VERSION = "0.0"
CVS = "cvs"
IS_RISCOS = false --detection logic todo.
CVS_EXPORT = "CVSROOT=:pserver:anonymous@riscosopen.org:/home/rool/cvsroot"
CVS_FLAGS = " -z9 "
CVS_ROOT =" :pserver:anonymous@riscosopen.org:/home/rool/cvsroot " --we always need the -d
LOCAL_ROOT = "../"
CVS_SETUP = CVS.." -d "..CVS_ROOT.." login"

--Set up to live in the Prepare directory.
MODULEDB = "../BuildSys/ModuleDB"
--MODULEDB = "testfile"
COMPONENTS_DIR = "../BuildSys/Components/ROOL/"
COMPONENTS_FILE = "AWH3" -- temporarily hardcoded.

modDB = {} --tablt to store module database lines.
componentsDB = {}
downloadsDB = {}
--print(CVS_SETUP)
--Put all code for main down bottom.
--====================================================================--
function testDir()
  os.execute("ls "..COMPONENTS_DIR)
end
--====================================================================--
function CVSLogin()
  ret = os.execute(CVS_SETUP)

  if (ret ==nil) then
    print("CVS not found. Exiting...")
    os.exit(1)
  end
end
--====================================================================--
function printModuleDB()
  --For debugging only.
  len = #modDB --only works when no holes in table!
  for i = 1, len do
    print(modDB[i]) --looking good.
  end
end
--====================================================================--
function printComponentsDB()
  --For debugging only.
  len = #componentsDB --only works when no holes in table!
  for i = 1, len do
    print(componentsDB[i]) --looking good.
  end
end
--====================================================================--
function printDownloadsDB()
  local len = 0
  local i = 0
  --For debugging only.
  len = #downloadsDB --only works when no holes in table!
  for i = 1, len do
    print(downloadsDB[i]) --looking good.
  end
end
--====================================================================--
--maybe it should return a table?
function readModuleDB()
  local curLine = nil --current line of file. Just for testing.
  local i = 0
--  print("entered readModuleDB")
--  file = io.open( MODULEDB, "r" )  

--  print(io.type(file))
--  if(file == nil) then
--    print("ModuleDB could not be opened.")
--    os.exit(1)
--  end
  
--  print(io.read())
  for line in io.lines (MODULEDB) do
  --check lines for comment
--    for i = 1, #line do
    for i = 1, string.len(line) do --beginning of scanning line
--     local chr = str:sub(i,i)
      local chr = string.sub(line, i, i)
     --need to check if only spaces before the #
      if(chr == "#")
      then
       flag = true
--       print("# detected. Skipping...") --comment out later.
       break
      else
     --good to write to table
      end --end if
    end --end i=1 #line for loop.
    if( flag == false)
    then    
      table.insert(modDB, line)
--      print(line) --sanitised output --
    end --endif
    flag = false --Is this the problem?
--    print ("Unsanitised "..line) --unsanitised output
  end
--  print("Leaving readModuleDB")
end --end of function readModuleDB
--====================================================================--
function readComponentsFile()
  --copypaste of readModuleDB
  local curLine = nil --current line of file. Just for testing.
  local i = 0
  local fullPath = ""
--  print("entered readComponentsFile")
  fullPath = COMPONENTS_DIR..COMPONENTS_FILE
  print(fullPath)
--[[
                FIXME FIXME FIXME
                ===== ===== =====
                
  Below doesn't fail gracefully if there is no file found!
  
  Need to try opening the file before uing io.type.
  Don_t forget to close it aftrward

]]--
  --surely there's a better way?
  file = io.open(fullPath, "r")
  
  if(file == nil) then
    print(COMPONENTS_FILE.." not found. Exiting...")
    os.exit(1)
  end
  io.close(file)
  
  for line in io.lines (fullPath) do
  --check lines for comment
--    for i = 1, #line do
    for i = 1, string.len(line) do --beginning of scanning line
--     local chr = str:sub(i,i)
      local chr = string.sub(line, i, i)
     --need to check if only spaces before the #
      if(chr == "#")
      then
        flag = true
--        print("# detected. Skipping...") --comment out later.
        break
      elseif(chr == "%")
      then
        flag = true
--        print("% detected. Skipping...")
        break
      elseif(chr == nil) --tried different things. Can't find one that works.
      then
        flag = true
--        print("Newline detected. Skipping...")
        break
      
      else
     --good to write to table
      end --end if
    end --end i=1 #line for loop.
    if( flag == false)
    then    
      table.insert(componentsDB, line)
--      print("-"..line) --sanitised output --
    end --endif
    flag = false --Is this the problem?
--    print (line) --unsanitised output
  end

end
--====================================================================--
function stripComponentsDB()
  
  local len = 0
  local tmpStr = ""
  local i, c = 0, 0
  --[[
  write to tmp string
  Churn through line char by char.
  1st space found, break and subtract 1 from char counter
  write back truncated string to table
  ]]--
  len = #componentsDB --only works when no holes in table!
  for i = 1, len do
    --print(componentsDB[i])
    tmpStr = componentsDB[i]
    for c = 1, string.len(tmpStr) do --beginning of scanning line
      local chr = string.sub(tmpStr, c, c)
      if( chr == " " ) then
        componentsDB[i] = string.sub( tmpStr, 1, c-1 )
--        print("componentsDB["..i.."]="..componentsDB[i])
      break;
      end
      
    end --end str scan loop  
  end --outer len loop

end
--====================================================================--
function findPath(componentName)
  local c, i = nil, nil
  s, f = nil, nil
  local chr = nil
  local oldchr = " "
  local colcount = 1
  pathString = nil
  local startchr, endchr = 0, 0 --for extracting the path
  tmpstr = ""
  tempstr2 = ""
  tmpidx = 0 --index entry
  idx = 0
--[[
takes the name as an argument. scans moduleDB for the component name,
then returns the path as a string.

FIXME:
FindPath is returning too much information.
]]--
  len = #modDB --only works when no holes in table!
  for tmpidx = 1, len do
    s = string.find(modDB[tmpidx], componentName)
    if( s ~= nil ) then
--      print("Found the component: "..modDB[tmpidx])
      tmpstr = modDB[tmpidx]
--      print("modDB Table entry # "..tmpidx)
      idx = tmpidx
      break; --found it!
    end
--    print(modDB[i])
  end --end for loop
  
  --i holds the index we need to scan
  --how do we count groups of spaces?
  --need the third column of text
--    tmpstr = modDB[i]
--    print("tmpstr = "..tmpstr) --we still have tmpstr
--    print("idx = "..idx) --not 0 once passed out. Not needed though.
    for c = 1, string.len(tmpstr) do --beginning of scanning line
--    for c = 1, #tmpstr do
      oldchr = chr
      chr = string.sub(tmpstr, c, c)
--      print("tmpstr = "..tmpstr) --spits out vast amounts of what it should.
      if( chr ~= " " and oldchr == " ") then --reached beginning of new column.
        --if(oldchr == " ") then
          colcount = colcount + 1
--          print("colcount = "..colcount)
          if(colcount == 3) then --c is _start_ position of what we want
            s = c --start char.
--            print("Start char is: "..s)
            f = c
            for f = c, #tmpstr do --intended to replace logic below for finding whitespace
--              print("f = "..f)
              chr = string.sub(tmpstr, f, f)
              if(chr == " ") then
                pathString = string.sub(tmpstr, s, f-1)     
              break; --we found the space
            end
          end

          
          --[[
          
            tmpstr2 = nil --that didn't help
            tmpstr2 = string.sub(tmpstr, c) --lifting substring. Humm.
            print("Path and extras = "..tmpstr2) --so far so good...
            --Now to trim off the trailing junk...
            print("tmpstr is "..string.len(tmpstr2).." characters long")
            for c = 1, string.len(tmpstr2) do
            --for c = 1, #tmpstr2 do 
              chr = string.sub(tmpstr2, c, c) --looking for a space
              if(chr == " ") then
              print("c is "..c)
               break; --found the end of what we need
              end --end if
            end --end inner loop
            --break gets us to here.
            --c = c - 1 --removing trailing space
            --c = c + 3 --!FIXME! Experiment. Line len is wrong. Data exists before truncation.
            pathString = string.sub(tmpstr2, 1, c) 
            print("Returning pathString: "..pathString)
            --working, but has stuff attached to the end that has to be removed.
           ]]--
            return (pathString) 
          end 
        --end
      --  modDB[i] = string.sub( tmpStr, 1, c-1 )
      --break;
      end
      
    end --end str scan loop
  return(nil)  
  
end
--====================================================================--
function getPaths()
  local tmp = ""
  local tmpPath = ""
  local len = 0
  local i = 0
--go through components DB a line at a time
--run findPath on each line
--save non nil results to downloadsDB
  len = #componentsDB
--  print("len = "..len)
  for i = 1, len do
    tmp = componentsDB[i]
    if(tmp ~= nil) then
--      print("tmp = "..tmp)
      tmpPath = findPath(tmp)
      if(tmpPath ~= nil) then
--        print("tmpPath = "..tmpPath)
        table.insert(downloadsDB, tmpPath)
      end
    end
  end
end

--====================================================================--
function RISCOSify()
--[[
 goes through and ensures download table dirs are delimited by .
 and changes the coded dirs up top to RO compatible format
]]--
end
--====================================================================--
function POSIXify()

--operates on downloadsDB. Calls functions to convert relative paths up top.
--not good enough. If already POSIXified it mangles ..
local tmpstr =""
  for l = 1, #downloadsDB do
    tmpstr = string.gsub(downloadsDB[l],'%.','/')  
    downloadsDB[l] = tmpstr
  end
--  MODULEDB = POSIXifyString(MODULEDB)
--  COMPONENTS_DIR = POSIXifyString(COMPONENTS_DIR)
--  LOCAL_ROOT = POSIXifyString(LOCAL_ROOT)
--[[
Same as above but / etc.
downloadsDB
]]--
end

--====================================================================--
function POSIXifyString(str)
  str = string.gsub(str, '%.', '/')
  str = string.gsub(str, '%^', '%.%.')
  return(str)
end
--====================================================================--
function downloadComponents()
--cvs checkout -d destination module

  Apachefy() --relocate this call. Just quicckly hacked in.
  if(IS_RISCOS == true) then
    os.execute("set CVSROOT :pserver:anonymous@riscosopen.org:/home/rool/cvsroot")
  else
    os.execute("CVSROOT=:pserver:anonymous@riscosopen.org:/home/rool/cvsroot") --doesn't work
    os.execute("export CVSROOT")
  end
  CVSLogin()
  
  --net churn through the list.
  for l = 1, #downloadsDB do
  --need to specify destination directory
  --FIXME cvs syntax is wrong. I can't get it.
  --os.execute(CVS..CVS_FLAGS..CVS_ROOT.." co "..downloadsDB[l].." -d -N"..LOCAL_ROOT)
  --os.execute(CVS..CVS_FLAGS.." -d "..LOCAL_ROOT.." co "..CVS_ROOT..downloadsDB[l]) 
--  os.execute("cvs -z9 -d ../ co -P "..downloadsDB[l])
  os.execute(CVS_EXPORT.." ; export CVSROOT ; cvs -z9 co -d ../ "..downloadsDB[l] )
  end
end
--====================================================================--
--changes castle to apache
function Apachefy()
  for l = 1, #downloadsDB do
   str = string.gsub(downloadsDB[l], "castle", "apache")
   downloadsDB[l] = str
   print(str)
  end 

end
--====================================================================--

--[[
The files I need are:
RISC_OS_Dev.BuildSys.Components.ROOL.xx
*Need way of detecting buildtree name
-List the directory into a table
-Have input as an arg. Check against table.
-  -l option to list table entries.
RISC_OS_Dev.BuildSys.ModuleDB

scan components file.
For each line, match ccolumn 3 of ModuleDB
Download dirs from col3 of moduleDB

]]--
--testDir() --tested. Result correct.
--CVSLogin()


--main
--use VERSION to detect OS and parse dirs accordingly.

for i = 1, #arg do
  if(arg[i] == "-v") then
    print("Grabber. Version "..VERSION)
    print("Using Lua version: ".._VERSION)
--print(arg[-3])
--print(arg[-2])
    print("The Lua executable is located at: "..arg[-1])

    os.exit(0)
  else
  --call function to check if string matches database...somehow.
  --for now, set the variable and let the file function catch it.
  COMPONENTS_FILE = arg[i]
  end


end --end of arg loop

readModuleDB()

--lines from this need to be matched with dirs from modDB
readComponentsFile()

stripComponentsDB()
--printComponentsDB()

getPaths()
--print("running printDownloadsDB")
--printDownloadsDB()

POSIXify()
printDownloadsDB()

downloadComponents()

--print("Testing FindPath")
--tmp = findPath("BootFX")
--if(tmp == nil) then
--  print("Entry not found")
--else
--  print("findPath returned: "..tmp)
--end
--printModuleDB()

os.exit(0)
--====================================================================--
